        var map;
        var lat=54.348792;
        var lng=18.532557;
        var zoom=16;
 
        function initialize() {
            var myOptions = {
                zoom: zoom,
                center: new google.maps.LatLng(lat, lng),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById('myMaps'), myOptions);
        }
 
        google.maps.event.addDomListener(window, 'load', initialize);

