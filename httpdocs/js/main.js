$(document).ready(function(){
    
    $('#myCarousel .carousel-inner div.item:first-child').addClass('active');
    $('.carousel').carousel({pause: null});
    
    $('.ajaxFilm').on('click',function(e){
           
           var id = $(this).attr('id');
           id = id.substr(5);
        
           $.ajax({
            type: "GET",
            url: "index.php?action=videoPlayer&movieId=" + id,
            data: {  }
            }).done(function( dataFromPhp ) {
                $('#myModal').modal('show');
                $('#myModal iframe').attr('src', dataFromPhp);
            });
        
        e.preventDefault();
        

   
    });
    
    
    $('#myModal button').click(function () {
        $('#myModal iframe').removeAttr('src');
    });
    
    
    $(function(){
        var stt_is_shown = false;
        $(window).scroll(function(){
            var win_height = 300;
            var scroll_top = $(document).scrollTop(); 
            if (scroll_top > win_height && !stt_is_shown) {
               stt_is_shown = true;
               $("#scroll-to-top").fadeIn();
               } else if (scroll_top < win_height && stt_is_shown) {
               stt_is_shown = false;
               $("#scroll-to-top").fadeOut();
               }
        });
        $("#scroll-to-top").click(function(e){
           e.preventDefault();
           $('html, body').animate({scrollTop:0}, 1500);
        });
     });
	
	var NavY = $('.nav').offset().top;
	 
	var stickyNav = function(){
	var ScrollY = $(window).scrollTop();
		  
	if (ScrollY > NavY) { 
		$('.menu').addClass('menuOnTop');
	} else {
		$('.menu').removeClass('menuOnTop'); 
	}
	};
	 
	stickyNav();
	 
	$(window).scroll(function() {
		stickyNav();
	});
    
    
    }
);
