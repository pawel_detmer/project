<?php

session_start();

require_once __DIR__ . '/../vendor/autoload.php';

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Sda\Project\Request\Request;
use Sda\Project\Template\Template;
use Sda\Project\Config\Config;
use Sda\Project\User\UserRepository;
use Sda\Project\Controller\Controller;
use Sda\Project\Film\FilmRepository;


$loader = new Twig_Loader_Filesystem(Config::TEMPLATE_DIR);
$twig = new Twig_Environment(
    $loader,
    ['cache' => false]
);

$template = new Template($twig);
$config = new Configuration();
$request = new Request();
$dbh = DriverManager::getConnection(Config::DB_CONNECTION_DATA, $config);
$repoUser = new UserRepository($dbh);
$repoFilm = new FilmRepository($dbh);

$app = new Controller($request, $repoUser, $template, $repoFilm);

$app->run();
/*tes hfg
 * asdasdgit sta
 * kljlasd
 */
