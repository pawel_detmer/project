<?php


namespace Sda\Project\Config;


/**
 * Class Routing
 * @package Sda\Project\Config
 */
class Routing
{

    const LOGIN = 'login';
    const REGISTER = 'register';
    const LOGOUT = 'logout';
    const MAIN_PAGE = 'main_page';
    const FILM = 'film';
    const CONTACT = 'kontakt';
    const VIDEO_PLAYER = 'videoPlayer';
    const SEARCHING_FILM = 'searchingFilm';
    const ADD_FILM = 'addFilm';

}
