<?php

namespace Sda\Project\Config;

/**
 * Class Config
 * @package Sda\Project\Config
 */
class Config
{
    const TEMPLATE_DIR = __DIR__ . '/../Template/templates';

    const DB_CONNECTION_DATA = [
        'dbname' => 'project',
        'user' => 'PawelD',
        'password' => '',
        'host' => 'localhost',
        'driver' => 'pdo_mysql',
        'charset' => 'utf8'
    ];
}
