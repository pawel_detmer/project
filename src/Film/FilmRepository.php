<?php

namespace Sda\Project\Film;

use Doctrine\DBAL\Connection;


/**
 * Class FilmRepository
 * @package Sda\Project\Film
 */
class FilmRepository
{
    /**
     * @var Connection
     */
    private $dbh;
    /**
     *
     */
    const PER_PAGE = 2;

    /**
     * FlightRepository constructor.
     * @param Connection $dbh
     */
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param int $page
     * @return array
     */
    public function getAllMovies($page = 1)
    {

        $sth = $this->dbh->prepare('SELECT * FROM `film` ORDER BY film_id DESC LIMIT :offset, :perPage');
        $sth->bindValue('offset', ($page * self::PER_PAGE - self::PER_PAGE), \PDO::PARAM_INT);
        $sth->bindValue('perPage', self::PER_PAGE, \PDO::PARAM_INT);
        $sth->execute();
        $movieData = $sth->fetchAll();

        return $movieData;
    }

    /**
     * @param $id
     * @return array
     */
    public function getMovieByPartOfTitle($id)
    {
        $sth = $this->dbh->prepare('SELECT * FROM `film` WHERE film_title LIKE "%' . $id . '%"');
        $sth->execute();
        return $sth->fetchAll();
    }

    /**
     * @return array
     */
    public function getLastAddMovie()
    {

        $sth = $this->dbh->prepare('SELECT * FROM `film` ORDER BY `film_time_stamp` DESC LIMIT 1');
        $sth->execute();
        return $sth->fetchAll();

    }

    /**
     * @return bool|string
     */
    public function countFilms()
    {
        $sth = $this->dbh->prepare('SELECT COUNT(*) FROM `film`  ');
        $sth->execute();
        return $sth->fetchColumn();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getMovieById($id)
    {
        $sth = $this->dbh->prepare('SELECT * FROM `film` WHERE `film_id` =:id');
        $sth->bindValue('id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $movieData = $sth->fetch();


        return $movieData;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getMovieByTitle($id)
    {
        $sth = $this->dbh->prepare('SELECT * FROM `film` WHERE `film_title` =:id');
        $sth->bindValue('id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $movieData = $sth->fetch();


        return $movieData;
    }

    /**
     * @return array
     */
    public function getMovieByTimeStamp()
    {
        $sth = $this->dbh->prepare('SELECT * FROM `film` ORDER BY `film_time_stamp` DESC LIMIT 3');
        $sth->execute();
        $threeLastAddMovie = $sth->fetchAll();

        return $threeLastAddMovie;
    }


    /**
     * @param Film $film
     */
    public function addNewFilm(Film $film)
    {
        $this->dbh->insert('film', [
            'film_title' => $film->getFilmTitle(),
            'film_content' => $film->getFilmContent(),
            'film_year' => $film->getFilmYear(),
            'film_url' => $film->getFilmUrl(),
            'film_type' => $film->getFilmType(),
            'film_link_imdb' => $film->getFilmLinkImdb(),
            'film_link_filmweb' => $film->getFilmLinkFilmweb(),
            'film_picture' => $film->getFilmPicture()
        ]);
    }

}