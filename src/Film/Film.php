<?php

namespace Sda\Project\Film;

class Film
{

    /**
     * @var
     */
    private $filmTitle;
    /**
     * @var
     */
    private $filmContent;
    /**
     * @var
     */
    private $filmYear;
    /**
     * @var
     */
    private $filmUrl;
    /**
     * @var
     */
    private $filmType;
    /**
     * @var
     */
    private $filmLinkImdb;
    /**
     * @var
     */
    private $filmLinkFilmweb;
    /**
     * @var
     */
    private $filmPicture;


    /**
     * Film constructor.
     * @param $filmTitle
     * @param $filmContent
     * @param $filmYear
     * @param $filmUrl
     * @param $filmType
     * @param $filmLinkImdb
     * @param $filmLinkFilmweb
     * @param $filmPicture
     */
    public function __construct($filmTitle, $filmContent, $filmYear, $filmUrl, $filmType, $filmLinkImdb, $filmLinkFilmweb, $filmPicture)
    {
        $this->filmTitle = $filmTitle;
        $this->filmContent = $filmContent;
        $this->filmYear = $filmYear;
        $this->filmUrl = $filmUrl;
        $this->filmType = $filmType;
        $this->filmLinkImdb = $filmLinkImdb;
        $this->filmLinkFilmweb = $filmLinkFilmweb;
        $this->filmPicture = $filmPicture;
    }

    /**
     * @return mixed
     */
    public function getFilmTitle()
    {
        return $this->filmTitle;
    }

    /**
     * @return mixed
     */
    public function getFilmContent()
    {
        return $this->filmContent;
    }

    /**
     * @return mixed
     */
    public function getFilmYear()
    {
        return $this->filmYear;
    }

    /**
     * @return mixed
     */
    public function getFilmUrl()
    {
        return $this->filmUrl;
    }

    /**
     * @return mixed
     */
    public function getFilmType()
    {
        return $this->filmType;
    }

    /**
     * @return mixed
     */
    public function getFilmLinkImdb()
    {
        return $this->filmLinkImdb;
    }

    /**
     * @return mixed
     */
    public function getFilmLinkFilmweb()
    {
        return $this->filmLinkFilmweb;
    }

    /**
     * @return mixed
     */
    public function getFilmPicture()
    {
        return $this->filmPicture;
    }


}
