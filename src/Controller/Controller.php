<?php

namespace Sda\Project\Controller;

use Sda\Project\Request\Request;
use Sda\Project\Template\Template;
use Sda\Project\User\UserRepository;
use Sda\Project\User\User;
use Sda\Project\Config\Routing;
use Sda\Project\Film\Film;
use Sda\Project\Film\FilmRepository;


/**
 * Class Controller
 * @package Sda\Project\Controller
 */
class Controller
{


    /**
     * @var Request
     */
    private $request;
    /**
     * @var UserRepository
     */
    private $repoUser;
    /**
     * @var Template
     */
    private $template;
    /**
     * @var array
     */
    private $params = [];
    /**
     * @var FilmRepository
     */
    private $repoFilm;

    /**
     * Controller constructor.
     * @param Request $request
     * @param UserRepository $repoUser
     * @param Template $template
     * @param FilmRepository $repoFilm
     */
    public function __construct(
        Request $request,
        UserRepository $repoUser,
        Template $template,
        FilmRepository $repoFilm
    )
    {
        $this->request = $request;
        $this->repoUser = $repoUser;
        $this->template = $template;
        $this->repoFilm = $repoFilm;
        $this->checkIfLogged();

    }

    private function checkIfLogged()
    {
        if (array_key_exists('activeUser', $_SESSION)) {
            $this->params['user'] = ucfirst(substr($_SESSION['email'], 0, strpos($_SESSION['email'], '@')));
        }
    }


    public function run()
    {


        $action = array_key_exists('action', $_GET) ? $_GET['action'] : 'main_page';
        $this->params['page'] = $action;


        switch ($action) {
            case Routing::MAIN_PAGE;
                $templateName = 'mainPage.html';
                $this->mainPage();
                break;
            case Routing::LOGIN;
                $templateName = 'login.html';
                $this->login();
                break;
            case Routing::REGISTER;
                $templateName = 'register.html';
                $this->register();
                break;
            case Routing::LOGOUT;
                $templateName = 'logout.html';
                $this->logout();
                break;
            case Routing::FILM;
                $templateName = 'film.html';
                $this->film();
                break;
            case Routing::CONTACT;
                $templateName = 'kontakt.html';
                break;
            case Routing::VIDEO_PLAYER;
                $this->videoPlayer();
                break;
            case Routing::SEARCHING_FILM;
                $templateName = 'searchingFilm.html';
                $this->searchingFilm();
                break;
            case Routing::ADD_FILM;
                $templateName = 'addFilm.html';
                $this->addFilm();
                break;
            default;
                echo '404 strona nie odnaleziona';
                break;
        }

        $this->template->renderTemplate($templateName, $this->params);

    }

    private function mainPage()
    {
        $this->params['threeLastAddFilms'] = array_reverse($this->repoFilm->getMovieByTimeStamp());
        $this->params['lastAddMovie'] = $this->repoFilm->getLastAddMovie();
    }

    private function videoPlayer()
    {
        $id = array_key_exists('movieId', $_GET) ? $_GET['movieId'] : 0;

        echo $this->repoFilm->getMovieById($id)['film_url'] . '?autoplay=1';
        exit();
    }


    private function login()
    {

        if (array_key_exists('email', $_POST) && array_key_exists('password', $_POST)) {

            if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $user = new User(htmlspecialchars($_POST['email']), htmlspecialchars($_POST['password']));
                $userData = $this->repoUser->getUserByEmail($user->getEmail());

                if ($userData['password'] === $user->getPassword()) {
                    $_SESSION['email'] = $user->getEmail();
                    $_SESSION['password'] = $user->getPassword();
                    $_SESSION['activeUser'] = 'Poprawnie zalogowany user';
                    $this->params['User'] = 'Zostałeś poprawnie zalogowany';
                    header('Location: index.php?action=main_page');

                } else {
                    $this->params['error'] = 'Niepoprawny login lub hasło';
                }
            } else {
                $this->params['errorEmail'] = 'Wpisano niepoprawny e-mail';
            }
        }
    }

    private function logout()
    {

        session_destroy();
        header("HTTP/1.1 301 Moved Permanently");
        header('Location: index.php?action=login');

    }

    public function register()
    {

        if (array_key_exists('email', $_POST) && array_key_exists('password', $_POST)) {
            if (filter_var($this->request->getParamFromPost('email'), FILTER_VALIDATE_EMAIL)) {
                $this->params['error'] = 'Podany adres e-mail:  ' . $_POST['email'] . ' - jest nieprawidłowy.\n';
                $userRegister = new User(
                    htmlspecialchars($this->request->getParamFromPost('email')),
                    htmlspecialchars($this->request->getParamFromPost('password'))
                );
                if (!$this->repoUser->getUserByEmail($userRegister->getEmail())) {
                    $this->params['error'] = 'Konto o podanym e-mailu już istnieje, podaj inny e-mail';

                }
                if ($_POST['secondPassword'] != $_POST['password']) {
                    $this->params['error'] = 'Podano różne hasła. Spróbuj jeszcze raz się zarejestrować';
                } else {
                    $this->repoUser->addUser($userRegister);
                    $_SESSION['email'] = $userRegister->getEmail();
                    $_SESSION['password'] = $userRegister->getPassword();
                    $_SESSION['activeUser'] = 'Poprawnie zalogowany user';
                    header('Location: index.php?action=main_page');
                }
            } else {
                if ($this->request->getParamFromPost('email') == NULL) {
                    $this->params['error'] = 'Nie podałeś e-maila. Aby się zarejestrować - wprowadź dane';
                } else {
                    $this->params['error'] = 'Podany adres e-mail:  ' .
                        $this->request->getParamFromPost('email') .
                        ' jest nieprawidłowy.';
                }
            }
        }
    }


    public function film()
    {

        if (array_key_exists('activeUser', $_SESSION)) {
            $page = $this->request->getParamFromGet('page', 1);

            $this->params['currentPage'] = $page;
            $this->params['allFilms'] = $this->repoFilm->getAllMovies($page);
            $this->params['pages'] = ceil($this->repoFilm->countFilms() / FilmRepository::PER_PAGE);

        } else {
            $this->params['error'] = 'Nie jesteś zalogowany. Aby móc oglądać filmy zaloguj lub zarejestruj się.';
        }
    }


    private function searchingFilm()
    {


        if (array_key_exists('activeUser', $_SESSION)) {

            $term = $this->request->getParamFromPost('term');
            if ($this->repoFilm->getMovieByPartOfTitle($term) == NULL
                || $this->request->getParamFromPost('term') == NULL
            ) {
                $this->params['error'] = 'Nie zaleziono filmu w bazie';
            } else {
                $this->params['searchingFilm'] = $this->repoFilm->getMovieByPartOfTitle(
                    $this->request->getParamFromPost('term')
                );
                $this->params['allFilms'] = $this->repoFilm->getMovieByPartOfTitle(
                    $this->request->getParamFromPost('term', '')
                );
            }

        } else {
            $this->params['error'] = 'Nie jesteś zalogowany. Aby móc wyszukiwać filmy zaloguj lub zarejestruj się.';

        }

    }



    private function addFilm()
    {

        if (array_key_exists('activeUser', $_SESSION) && $_SESSION['activeUser'] != NULL) {

            if (array_key_exists('film_title', $_POST)) {
                $fileName = $_FILES["film_picture"]['name'];
                $uploads_dir = __DIR__ . '\..\..\httpdocs\images\\';
                move_uploaded_file($_FILES['film_picture']['tmp_name'], $uploads_dir . $fileName);
                $filmAdd = new Film(
                    htmlspecialchars($this->request->getParamFromPost('film_title', 'unknown')),
                    htmlspecialchars($this->request->getParamFromPost('film_content', 'unknown')),
                    htmlspecialchars($this->request->getParamFromPost('film_year', 'unknown')),
                    htmlspecialchars($this->request->getParamFromPost('film_url', 'unknown')),
                    htmlspecialchars($this->request->getParamFromPost('film_type', 'unknown')),
                    htmlspecialchars($this->request->getParamFromPost('film_link_imdb', 'unknown')),
                    htmlspecialchars($this->request->getParamFromPost('film_link_filmweb', 'unknown')),
                    htmlspecialchars($this->request->getParamFromPost('film_picture', '/images/' . $fileName))
                );
                $this->repoFilm->addNewFilm($filmAdd);
            }

        } else {
            $this->params['error'] = 'Nie jesteś zalogowany. Nie możesz dodać filmu.';
        }
    }
}