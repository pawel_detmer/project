<?php

namespace Sda\Project\User;


use Doctrine\DBAL\Connection;
use Sda\Project\User\UserNotFoundException;
use Sda\Project\User\User;
use Sda\Project\Film\Film;
use Sda\Project\Controller\Controller;


class UserRepository {
        /**
     * @var Connection
     */
    private $dbh;

    /**
     * UserRepository constructor.
     * @param Connection $dbh
     */
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }
    
    public function getAllUsers () {
         $sth = $this->dbh->prepare('SELECT * FROM `users`');
         $sth->execute();
         $allUsers = $sth->fetchAll();
         
         if (false === $allUsers) {
           throw new PhaseNotFoundException();
         }
     
         return UserRepository::makeFromRepository($allUsers);
         
    }
   
    public function getUserByEmail ($email) {
         $sth = $this->dbh->prepare('SELECT * FROM `users` WHERE `email` =:email');
         $sth->bindValue('email', $email, \PDO::PARAM_INT);
         $sth->execute();
         $emailData = $sth->fetch();
         
         return $emailData;
    }
    
    public function addUser(User $user) {
     
        $sth = $this->dbh->prepare('SELECT * FROM `users` WHERE `email` = :email');
        $sth->bindValue('email', $user->getEmail() , \PDO::PARAM_INT);
        $sth->execute();
        $userData = $sth->fetch();
        if(false === $userData){
            return $this->dbh->insert('users', ['email'=>$user->getEmail(), 'password'=>$user->getPassword()]);
        } else {
            echo 'uzytkownik o podaym loginie istnieje';
       }
    
    }
    
}
