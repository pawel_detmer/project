<?php

namespace Sda\Project\User;

use Sda\Project\User\User;


class UserFactory {
    public static function makeFromRepository(array $UserData)
        {
            return new User(
                    $UserData['email'],
                    $UserData['password']
                    );
        }
}
